# Kazidomi technical test
## Create a branch with NAME-FIRSTNAME
## Create a entity "Blog" with :
* ID
* Title translatable (with easy system to add a language)
  *EN
  *FR
* Content
* Date add (automatically defined on add)
* Date update (null if never updated, automatically defined on update)
## This entity must be :
* Added by a form with cool editor for content
* Listed on homepage
* See with clean dedicated URL /blog/title
* Delete on ajax (soft - no hard deletion) from list
## This code must be tested :
* With PHP unit testing
## This code should be tested :
* With Behat
# Create a merge resquest


Note : Design is not important here but clean code yes.

Thanks and good luck :-)

